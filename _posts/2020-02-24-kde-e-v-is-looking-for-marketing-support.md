---
title: 'KDE e.V. is looking for marketing support'
date: 2020-02-24 00:00:00 
layout: post
---

> Edit 2020-03-04: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a marketing professional to help KDE improve its marketing and grow the marketing team. Please see the <a href="https://ev.kde.org/resources/marketingsupport-callforproposals-2020.pdf">call for proposals</a> for more details about this contract opportunity. We are looking forward to your application.
      
