---
title: 'KDE e.V. Quarterly Report 2014 Q4'
date: 2015-10-19 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2014_Q4.pdf">report for the fourth quarter of 2014</a>.

This report covers October through December 2014, including statements from the board, reports from sprints and a feature article about the KDE participation in Google Summer of Code 2014.
