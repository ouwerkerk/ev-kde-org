---
title: 'KDE e.V. Community Report 2019'
date: 2020-09-08 10:42:04 
layout: post
---


2019 was an exciting year for KDE and its community. Once more, we delivered excellent Free Software for the world to enjoy and we did it as a great team that can achieve beautiful things together.

You can find the report here: [KDE e.V. Community Report for 2019](https://ev.kde.org/reports/ev-2019/).
