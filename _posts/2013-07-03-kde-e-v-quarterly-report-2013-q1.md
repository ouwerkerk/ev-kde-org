---
title: 'KDE e.V. Quarterly Report 2013 Q1'
date: 2013-07-03 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2013_Q1.pdf">first quarterly report of 2013</a>.

This report covers January through March 2013, including statements from the board, reports from sprints and a feature about KDE India.
      
