---
title: 'KDE e.V. Membership Assembly 2006'
date: 2006-09-01 00:00:00 
layout: post
---

The annual general membership assembly of the KDE e.V. 2006
took place in Dublin during <a href="http://conference2006.kde.org">aKademy 2006</a>.
The board was extended to five members with Adriaan de Groot and Sebastian K&#252;gler as new
members while Mirko B&#246;hm stepped down after serving as vice president and
treasurer for seven years. There also were taken decisions to accept the
rules of procedures about online voting, to accept the System
Administration team as an official KDE e.V. working group and more.
      
